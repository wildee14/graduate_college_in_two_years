import React from "react";
import { Link } from "react-router-dom";
import AppRouter from "../routers/AppRouter";

const HeaderImage = props => (
  <div className="header-image center">
    <h1 className="header-text white-text">
      Shorten College & Experience More
    </h1>
  </div>
);

export default HeaderImage;
